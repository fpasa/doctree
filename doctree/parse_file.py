import ast
from ast import (
    parse,
    Module as ASTModule,
    Expr,
    Constant,
    Assign,
    AnnAssign,
    FunctionDef,
    AsyncFunctionDef,
    ClassDef,
    arg as Arg,
    stmt as Statement,
    Name,
    Subscript,
)
from pathlib import Path
from typing import Union, Optional, Literal, Any, TypeVar

from doctree.doctree import Module, Function, Argument


T = TypeVar("T")


def parse_file(path: Path) -> Module:
    ast = parse(path.read_text())
    return _parse_ast(ast)


def _parse_ast(ast: ASTModule) -> Module:
    # Extract module docstring
    module = Module(name="", doc=_get_docstring(ast.body))

    for stmt in ast.body:
        # if isinstance(stmt, (Assign, AnnAssign)):
        #     assert len(stmt.targets)
        #     print(stmt.targets, stmt.value, stmt.type_comment)

        if isinstance(stmt, (FunctionDef, AsyncFunctionDef)) and _is_public(stmt.name):
            module.functions.append(_parse_func(stmt))

    return module


def _parse_func(func: Union[FunctionDef, AsyncFunctionDef]) -> Function:
    return Function(
        name=func.name,
        doc=_get_docstring(func.body),
        arguments=[
            *[_parse_argument(arg, kind="positional") for arg in func.args.posonlyargs],
            *[
                _parse_argument(arg, default)
                for arg, default in zip(
                    func.args.args,
                    # Pad defaults with None, because defaults only
                    # contains the last n items which have a default value
                    _pad_start(func.args.defaults, len(func.args.args)),
                )
            ],
            *(
                [_parse_argument(func.args.vararg, kind="vararg")]
                if func.args.vararg is not None
                else []
            ),
            *(
                [_parse_argument(func.args.kwarg, kind="vararg_kwarg")]
                if func.args.kwarg is not None
                else []
            ),
            *[
                _parse_argument(arg, default, kind="kwarg")
                for arg, default in zip(func.args.kwonlyargs, func.args.kw_defaults)
            ],
        ],
        asynchronous=isinstance(func, AsyncFunctionDef),
    )


def _parse_argument(
    arg: Arg,
    default: Optional[ast.Expr] = None,
    kind: Literal[None, "positional", "kwarg", "vararg", "vararg_kwarg"] = None,
) -> Argument:
    return Argument(
        name=arg.arg,
        type=_type_to_string(arg.annotation),
        kind=kind,
        optional=default is not None,
        default_value=_get_value(default),
    )


def _get_docstring(body: list[Statement]) -> Optional[str]:
    first_stmt = body[0]
    if (
        isinstance(first_stmt, Expr)
        and isinstance(first_stmt.value, Constant)
        and isinstance(first_stmt.value.value, str)
    ):
        return first_stmt.value.value
    return None


def _is_public(name: str) -> bool:
    return not name.startswith("_")


def _pad_start(items: list[T], length: int) -> list[Optional[T]]:
    len_diff = length - len(items)
    return [*[None] * len_diff, *items]


def _type_to_string(annotation: Optional[Expr]) -> Optional[str]:
    if annotation is None:
        return None
    elif isinstance(annotation, Name):
        return annotation.id
    elif isinstance(annotation, Subscript):
        return (
            f"{_type_to_string(annotation.value)}[{_type_to_string(annotation.slice)}]"
        )

    raise NotImplementedError(f"Parsing type: {annotation}")


def _get_value(value: Optional[Expr]) -> Optional[str]:
    if value is None:
        return None
    elif isinstance(value, Constant):
        return value.value

    raise NotImplementedError(f"Parsing value: {value}")
