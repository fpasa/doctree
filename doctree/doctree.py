from __future__ import annotations

from dataclasses import dataclass, field
from typing import Optional, Any, Literal


@dataclass
class Documentation:
    modules: list[Module] = field(default_factory=list)


@dataclass
class Module:
    name: str
    doc: Optional[str] = None
    constants: list[Variable] = field(default_factory=list)
    functions: list[Function] = field(default_factory=list)
    classes: list[Class] = field(default_factory=list)


@dataclass
class Class:
    name: str
    constructor: Function
    doc: Optional[str] = None
    variables: list[Variable] = field(default_factory=list)
    methods: list[Function] = field(default_factory=list)


@dataclass
class Variable:
    name: str
    default_value: Any
    doc: Optional[str] = None
    constant: bool = False


@dataclass
class Function:
    name: str
    doc: Optional[str] = None
    arguments: list[Argument] = field(default_factory=list)
    return_doc: Optional[str] = None
    errors: list[Error] = field(default_factory=list)
    asynchronous: bool = False
    decorator: bool = False


@dataclass
class Argument:
    name: str
    type: Optional[str] = None
    doc: Optional[str] = None
    optional: bool = False
    default_value: Any = None
    kind: Literal[None, "positional", "kwarg", "vararg", "vararg_kwarg"] = None


@dataclass
class Error:
    type: Optional[str] = None
    doc: Optional[str] = None
