from pathlib import Path
from pprint import pprint

from doctree.doctree import Module, Function, Argument
from doctree.parse_file import parse_file


def test_parse_file(data_path: Path) -> None:
    module = parse_file(data_path / "example.py")
    assert module == Module(
        name="",
        doc="Example module docstring",
        functions=[
            Function(
                name="my_func",
                doc="my_func docstring",
                arguments=[
                    Argument(name="a"),
                    Argument(name="b"),
                ],
            ),
            Function(
                name="my_typed_func",
                arguments=[
                    Argument(name="a", type="int"),
                    Argument(name="b", type="int"),
                ],
            ),
            Function(
                name="my_async_func",
                arguments=[
                    Argument(name="a"),
                    Argument(name="b"),
                ],
                asynchronous=True,
            ),
            Function(
                name="my_typed_async_func",
                doc="my_typed_async_func docstring",
                arguments=[
                    Argument(name="a", type="int"),
                    Argument(name="b", type="int"),
                ],
                asynchronous=True,
            ),
            Function(
                name="complex_signature",
                arguments=[
                    Argument(name="pos1", type="str", kind="positional"),
                    Argument(name="pos2", kind="positional"),
                    Argument(name="norm", type="float"),
                    Argument(name="opt", type="int", optional=True, default_value=3),
                    Argument(name="kw1", type="int", kind="kwarg"),
                    Argument(
                        name="kw2",
                        type="Optional[bool]",
                        kind="kwarg",
                        optional=True,
                        default_value=None,
                    ),
                ],
            ),
            Function(
                name="my_vararg_func",
                arguments=[
                    Argument(name="args", kind="vararg"),
                    Argument(name="kwargs", kind="vararg_kwarg"),
                ],
            ),
        ],
    )
