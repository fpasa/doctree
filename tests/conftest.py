from pathlib import Path

from pytest import fixture

ROOT_PATH = Path(__file__).parent.parent


@fixture
def data_path() -> Path:
    return ROOT_PATH / "data"
