"""Example module docstring"""
from typing import Optional

MY_CONSTANT = 123
MY_TYPED_CONSTANT: float = 123
_PRIVATE_CONSTANT = 456


def my_func(a, b):
    """my_func docstring"""
    return a + b


def _my_private_func(a, b):
    return a + b


def my_typed_func(a: int, b: int) -> int:
    return a + b


async def my_async_func(a, b):
    return a + b


async def my_typed_async_func(a: int, b: int) -> int:
    """my_typed_async_func docstring"""
    return a + b


def complex_signature(
        pos1: str,
        pos2,
        /,
        norm: float,
        opt: int = 3,
        *,
        kw1: int,
        kw2: Optional[bool] = None,
):
    ...


def my_vararg_func(*args, **kwargs):
    pass
